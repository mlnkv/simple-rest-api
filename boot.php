<?php

define('ROOT', realpath(dirname(__FILE__)) . '/');
define('STORAGE', ROOT . 'storage/');

require "libs/jsondb.php";

function collection($name) {
  return new Jsondb($name);
}

