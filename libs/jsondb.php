<?php

class Jsondb {

  private $name = null;
  private $path = null;

  private $settings = null;

  private $items = null;

  private $where = null;

  public function __construct($name) {
    $this->name = $name;
    $this->path = STORAGE . $this->name . '.json';
  }

  public function saveEntry($entry) {
    $this->_readFile();
    $entry['_uid'] = uniqid();

    $this->items[] = $entry;

    if ($this->_writeFile()) {
      return $entry;
    }

    return null;
  }

  public function find($where = null) {

    $this->where = $where;

    return $this;
  }

  public function update($data) {

    $this->_readFile();

    if ($this->where) {

      if (is_array($this->where)) {

        foreach ($this->where as $where_key => $where_val) {
          foreach ($this->items as $item_key => $item_value) {
            if (isset($this->items[$item_key][$where_key]) &&
              $this->items[$item_key][$where_key] == $where_val) {
              foreach ($data as $data_key => $data_value) {
                $this->items[$item_key][$data_key] = $data_value;
              }
            }
          }
        }
      }

      if (is_callable($this->where)) {
        foreach (array_filter($this->items, $this->where) as $item_key => $item_value) {
          foreach ($data as $data_key => $data_value) {
            $this->items[$item_key][$data_key] = $data_value;
          }
        }
      }

      if (is_string($this->where)) {
        foreach ($this->items as $item_key => $item_value) {
          if ($this->items[$item_key]['_uid'] == $this->where) {
            foreach ($data as $data_key => $data_value) {
              $this->items[$item_key][$data_key] = $data_value;
            }
          }
        }
      }
    } else {
      foreach ($this->items as $item_key => $item_value) {
        foreach ($data as $data_key => $data_value) {
          $this->items[$item_key][$data_key] = $data_value;
        }
      }
    }

    $this->_writeFile();

    return $this;
  }

  public function delete() {
    $this->_readFile();

    if ($this->where) {

      if (is_array($this->where)) {

        foreach ($this->where as $where_key => $where_val) {
          foreach ($this->items as $item_key => $item_value) {
            if (isset($this->items[$item_key][$where_key]) &&
              $this->items[$item_key][$where_key] == $where_val) {
              unset($this->items[$item_key]);
            }
          }
        }
      }

      if (is_callable($this->where)) {
        foreach (array_filter($this->items, $this->where) as $item_key => $item_value) {
          unset($this->items[$item_key]);
        }
      }

      if (is_string($this->where)) {
        foreach ($this->items as $item_key => $item_value) {
          if ($this->items[$item_key]['_uid'] == $this->where) {
            unset($this->items[$item_key]);
          }
        }
      }

      $this->items = array_values($this->items);
    } else {
      $this->items = [];
    }

    $this->_writeFile();

    return $this;
  }

  public function toArray() {
    $this->_readFile();

    if ($this->where) {

      if (is_array($this->where)) {
        foreach ($this->where as $key => $val) {
          $this->items = array_filter($this->items, function($item) use ($key, $val) {
            return isset($item[$key]) && $item[$key] == $val;
          });
        }
      }

      if (is_callable($this->where)) {
        $this->items = array_filter($this->items, $this->where);
      }

      if (is_string($this->where)) {
        $this->items = array_filter($this->items, function($item) {
          return $item['_uid'] == $this->where;
        });
      }
    }

    return array_values($this->items);
  }

  public function settings($key = null, $value = null) {
    $this->_readFile();

    if ($key === null) {
      return $this->settings;
    }

    if ($value === null) {
      return isset($this->settings[$key]) ? $this->settings[$key] : null;
    }

    $this->settings[$key] = $value;

    $this->_writeFile();

    return true;
  }

  private function _readFile() {
    $json = file_get_contents($this->path);
    $data = json_decode($json, true);
    $this->settings = $data['settings'];
    $this->items = $data['items'];
  }

  private function _writeFile() {
    $this->settings['updated'] = date('Y-m-d H:i:s');

    $data = [
      'settings' => $this->settings,
      'items' => $this->items
    ];

    $json = json_encode($data);

    $fp = fopen($this->path, 'r+');

    if (flock($fp, LOCK_EX)) {
      ftruncate($fp, 0);
      fwrite($fp, $json);
      fflush($fp);
      flock($fp, LOCK_UN);
    } else {
      return false;
    }

    fclose($fp);

    return true;
  }

  static function exists($name) {
    return file_exists(STORAGE . $name . '.json');
  }

  static function create($name) {
    if (static::exists($name)) return false;
    $data = [
      'settings' => [
        'created' => date('Y-m-d H:i:s'),
        'updated' => date('Y-m-d H:i:s')
      ],
      'items' => []
    ];

    $json = json_encode($data);

    file_put_contents(STORAGE . $name . '.json', $json);
  }
}

?>