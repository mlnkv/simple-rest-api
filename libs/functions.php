<?php

function dd() {
  echo '<pre>';
  call_user_func_array('var_dump', func_get_args());
  exit;
}

function request($key, $fallback = null) {
  static $request = null;
  if ($request === null)
    $request = request_body();
  return isset($request[$key]) ? $request[$key] : $fallback;
}