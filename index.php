<?php

define('ROOT', realpath(dirname(__FILE__)) . '/');
define('STORAGE', ROOT . 'storage/');

require ROOT . 'libs/dispatch.php';
require ROOT . 'libs/jsondb.php';
require ROOT . 'libs/functions.php';
require ROOT . 'configs/configs.php';

function isAuth() {
  return !!session('authenticated_user_id');
}

on('GET', '/', function() {
  if (!isAuth()) redirect(config('dispatch.url') . 'login');
  render('dashboard', null, false);
});

on(['GET'], '/login', function() {
  render('login', null, false);
});

on(['POST'], '/login', function() {
  $response = ['status' => 'ok'];

  if (request('login')) {
    $db = new Jsondb('.users');
    $user = $db->find(['login' => request('login')])->toArray()[0];
    if (!$user || hash('sha256', $user['salt'] . hash('sha256', request('password'))) !== $user['hash'])
      $response['status'] = 'error';
    else {
      session('authenticated_user_id', $user['uid']);
    }
  }

  json_out($response);
});

on('GET', '/logout', function() {
  session('user', null);
  redirect(config('dispatch.url'));
});

on('GET', '/collections', function() {
  $response = [
    'status' => 'ok',
    'data' => null
  ];

  $collections = new Jsondb('.collections');
  $response['data'] = $collections->find()->toArray();

  json_out($response);
});

on('GET', '/:name(/:uid)*', function($name, $uid = null) {

  if (Jsondb::exists($name)) {
    $db = new Jsondb($name);

    if ($uid) {
      scope('data', $db->find($uid)->toArray());
    } else {
      scope('data', $db->find()->toArray());
    }
  }

  json_out([
    'status' => scope('status'),
    'data' => scope('data')
  ]);
});

on('POST', '/:name(/:uid)*', function($name, $uid = null) {

  if (Jsondb::exists($name)) {
    $db = new Jsondb($name);

    $data = json_decode(request_body(), true);

    if ($uid) {

      if ($data) {
        if ($result = $db->find($uid)->update($data)->toArray()) {
          scope('data', $result);
        } else {
          scope('status', 'error');
        }
      } else {
        $db->find($uid)->delete();
      }

    } else {

      $new_entry = $db->saveEntry($data);

      if ($new_entry) {
        scope('data', [$new_entry]);
      } else {
        scope('status', 'error');
      }
    }
  }

  json_out([
    'status' => scope('status'),
    'data' => scope('data')
  ]);
});

dispatch();

?>