<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>My App</title>
  <link rel="stylesheet" href="assets/css/scooter.css">
  <link rel="stylesheet" href="assets/css/style.css">
  <style>
    @keyframes loginFormIn {
      0% {
        opacity: 0;
        transform: translate(0, -20px);
      }
      100% {
        opacity: 1;
        transform: translate(0, 0);
      }
    }

    @keyframes loginFormOut {
      0 {
        opacity: 1;
        transform: translate(0, 0);
      }
      100% {
        opacity: 0;
        transform: translate(0, 20px);
      }
    }

    .login {
      width: 220px;
      margin: 100px auto 0;
      animation: loginFormIn 0.8s forwards;
    }

    .login.out {
      animation: loginFormOut 0.8s forwards;
    }
  </style>
</head>
<body>

  <div class="login">
    <form id="form">
      <label class="c-label">
        Login
        <input type="text" class="c-input" id="login" required>
      </label>
      <label class="c-label">
        Password
        <input type="password" class="c-input" id="password" required>
      </label>
      <div class="u-font-center">
        <button class="c-btn c-btn--secondary">Enter</button>
      </div>
    </form>
  </div>

  <script src="assets/js/utils.js"></script>
  <script>
    var wrap =  document.querySelector(".login"),
      formElement = document.querySelector("#form"),
      loginInput = document.querySelector("#login"),
      passwordInput = document.querySelector("#password"),
      submitButton = formElement.querySelector("button")

    formElement.addEventListener("submit", formSubmitHandler)

    function formSubmitHandler(event) {
      event.preventDefault()
      submitButton.classList.add('c-btn--loader')
      submitButton.setAttribute('disabled', true)
      var authData = {
        login: loginInput.value,
        password: passwordInput.value
      }
      time = Date.now()
      request('POST', 'login', authData, function(error, json) {
        setTimeout(function() {
          responseHandler(error, json)
        }, 400)
      })
    }

    function responseHandler(error, json) {
      if (json.status == 'error') {
        loginInput.classList.add('is-invalid')
        passwordInput.classList.add('is-invalid')
        submitButton.classList.remove('c-btn--loader')
        submitButton.removeAttribute('disabled')
      } else {
        wrap.classList.add('out');
        setTimeout(function() {
          location.href = "./"
        }, 800)
      }
    }
  </script>
</body>
</html>