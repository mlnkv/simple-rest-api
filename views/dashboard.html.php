<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>My App</title>
  <link rel="stylesheet" href="assets/css/scooter.css">
  <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>

  <app></app>

  <script type="riot/tag" src="assets/tags/app.html"></script>
  <script type="riot/tag" src="assets/tags/collections.html"></script>
  <script type="riot/tag" src="assets/tags/settings.html"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/riot/2.3.13/riot+compiler.min.js"></script>
  <script src="assets/js/utils.js"></script>
  <script>riot.compile(function() { riot.mount('*') })</script>
</body>
</html>