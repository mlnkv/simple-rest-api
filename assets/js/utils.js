function request(method, url, data, callback) {
  var xhttp = new XMLHttpRequest()
  xhttp.open(method, url, true)
  xhttp.setRequestHeader("Content-type", "application/json")
  if (callback) {
    xhttp.onload = function() {
      if (xhttp.status >= 200 && xhttp.status < 400) {
        try {
          callback(null, JSON.parse(xhttp.responseText))
        } catch(e) {
          callback({type: "parse", text: e})
        }
      } else {
        callback({type: "status", text: xhttp.status})
      }
    }
    xhttp.onerror = function(e) {
      callback({type: "error", text: e})
    }
  }
  xhttp.send(data && JSON.stringify(data) || null)
}